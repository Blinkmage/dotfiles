# Dotfiles

These are my configuration files Vim, Git, Irb, Ack and more.

## Installation

Use the [homesick gem](https://github.com/technicalpickles/homesick) to install and symlink all files to their proper location:

    $ gem install homesick
    $ homesick clone https://gitlab.com/biox/dotfiles.git
    $ homesick symlink dotfiles
    $ cd ~/.homesick/repos/dotfiles
    $ brew bundle

A _huge_ thanks to [Arjan van der Gaag](https://github.com/avdgaag) for the [original](https://github.com/avdgaag/dotfiles) set of dotfiles I based
mine off of!!
